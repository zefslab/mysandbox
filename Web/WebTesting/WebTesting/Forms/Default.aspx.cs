﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTesting
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitComplete += btnCheckBrowserInfo_Click;
        }

        protected void btnCheckBrowserInfo_Click(object sender, EventArgs e)
        {
            string info = "";
            info += $"<li>Is the client AOL? {Request.Browser.AOL}</li>" +
                $"<li>Does the client support ActiveX? {Request.Browser.ActiveXControls}</li>";

            lblOutput.Text = info;
            var test = ViewState[""];
            //Response.Redirect("http://kemerovo.hh.ru/vacancy/15330951?query=c%23%20.net");
            Server.Transfer("About.aspx");
            var exp = Server.GetLastError();
            Server.ClearError();
        }
    }
}