﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTesting.Forms
{
    public partial class PresentationState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Заполнить ListBox динамически!
                lbMyList.Items.Add("Item One");
                lbMyList.Items.Add("Item Two");
                lbMyList.Items.Add("Item Three");
                lbMyList.Items.Add("Item Four");
            }
            else
            {
                Label1.Text = (string) ViewState["LableText"];
            }

           

        }

        protected void btnFill_Click(object sender, EventArgs e)
        {
            ViewState["LableText"] = "All succesfull";
            throw new Exception("Это моя сгенерированная ошибка.");
        }
    }
}