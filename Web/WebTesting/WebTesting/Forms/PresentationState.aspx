﻿<%@ Page Language="C#" AutoEventWireup="true" 
    CodeBehind="PresentationState.aspx.cs" 
    Inherits="WebTesting.Forms.PresentationState"
     %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ListBox ID="lbMyList" runat="server" EnableViewState="False" >
               
        </asp:ListBox>
        <br />
        <asp:Button ID="btnFill" runat="server" Text="Button" OnClick="btnFill_Click" />
        
    
    </div>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </form>
</body>
</html>
