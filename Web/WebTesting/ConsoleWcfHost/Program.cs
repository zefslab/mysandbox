﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WcfServiceLibrary;

namespace ConsoleWcfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost serviceHost = new ServiceHost(typeof (Service1)))
            {
                serviceHost.Open();
            }

            Console.WriteLine("Service opened");
            Console.ReadKey();
        }
    }
}
