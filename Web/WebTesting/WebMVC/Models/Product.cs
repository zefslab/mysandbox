﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebMVC.Models
{
    public class Product
    {
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Кол-во")]
        public int Count { get; set; }

        [DisplayName("Цена")]
        public decimal Cost { get; set; }
    }
}