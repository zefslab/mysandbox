﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Products(int count)
        {
            var products = new Product[]
            {
                new Product
                {
                    Name = "Сковорорда",
                    Count = 12,
                    Cost = 2300
                },
                new Product
                {
                    Name = "Крышка",
                    Count = 10,
                    Cost = 1300
                },
                new Product
                {
                    Name = "Нож",
                    Count = 22,
                    Cost = 300
                },
                new Product
                {
                    Name = "Стиральная машина",
                    Count = 12,
                    Cost = 21300
                },
                new Product
                {
                    Name = "Телевизор",
                    Count = 10,
                    Cost = 13000
                },
                new Product
                {
                    Name = "Мультимедиа центр",
                    Count = 22,
                    Cost = 30000
                }
            };
            return View(products.Take(count).ToArray());
        }


        public ActionResult Products()
        {
            ViewBag.Message = "Products";

            return View(new Product[] {
                new Product
                {
                    Name = "Сковорорда",
                    Count = 12,
                    Cost =  2300
                },
                new Product
                {
                    Name = "Крышка",
                    Count = 10,
                    Cost =  1300
                },
                new Product
                {
                    Name = "Нож",
                    Count = 22,
                    Cost =  300
                },
                new Product
                {
                    Name = "Стиральная машина",
                    Count = 12,
                    Cost =  21300
                },
                new Product
                {
                    Name = "Телевизор",
                    Count = 10,
                    Cost =  13000
                },
                new Product
                {
                    Name = "Мультимедиа центр",
                    Count = 22,
                    Cost =  30000
                }
            });
        }
    }
}