﻿using System;
using ConsoleApplication1.Behaviors;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var xmlData = "<PASP>" +
                          "<LastName>Буйвол</LastName>" +
                          "<FirstName>Тамара</FirstName>" +
                          "<MiddleName>Васильевна</MiddleName>" +
                          "<DateBirth>1951-03-13</DateBirth>" +
                          "<Gender>Female</Gender>" +
                          "</PASP>";
            ///Установили поведение парсинга
            var ServicePersone = new PersonService();

            ServicePersone.Parse(new PersonBaseParseSemd(), xmlData);
            
            ///Изменили поведение парсинга
            ServicePersone.Parse(new PersonBaseParseUrish(), xmlData);

            ///Добавил еще одно поведение парсинга
            ServicePersone.Parse(new PersonBaseParseAggrived(), xmlData);
            Console.ReadKey();
        }
    }
}
