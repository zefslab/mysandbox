﻿using ConsoleApplication1.Data;

namespace ConsoleApplication1.Behaviors
{
    public class PersonBaseParseAggrived : BaseParseBehavior<PersonDto>
    {
        protected override PersonDto _parseGeneral(string xmlData)
        {
            return new PersonDto("PersonBaseParseAggrived");
        }
    }

   
}
