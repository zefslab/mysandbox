﻿using ConsoleApplication1.Data;

namespace ConsoleApplication1
{
    public abstract class BaseParseBehavior<TDto>
        where TDto : BaseImportDto
    {
        public TDto Parse(string xmlData)
        {




            return _parseGeneral(xmlData);
        }

        protected abstract TDto _parseGeneral(string xmlData);
    }
}
