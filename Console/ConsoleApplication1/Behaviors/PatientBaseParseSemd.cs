﻿using ConsoleApplication1.Data;

namespace ConsoleApplication1.Behaviors
{
    public class PatientBaseParseSemd : BaseParseBehavior<PatientDto>
    {
       

        protected override PatientDto _parseGeneral(string xmlData)
        {
            return new PatientDto("PatientParseSemd");
        }
    }

   
}
