﻿using ConsoleApplication1.Data;

namespace ConsoleApplication1.Behaviors
{
    public class MaintenanceBaseParseSemd : BaseParseBehavior<MaintenanceDto>
    {
        protected override MaintenanceDto _parseGeneral(string xmlData)
        {
            return new MaintenanceDto("MaintenanceParseSemd");
        }
    }

   
}
