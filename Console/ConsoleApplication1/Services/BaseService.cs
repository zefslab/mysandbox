﻿using ConsoleApplication1.Data;

namespace ConsoleApplication1.Services
{
    public abstract class BaseService<TDto>
        where TDto : BaseImportDto
    {
        /// <summary>
        /// Вариант 2. Установка поведения через дополнительный метод
        /// </summary>
        /// <param name="baseParseBehavior"></param>
        public void  Parse(BaseParseBehavior<TDto> baseParseBehavior, string xmlData)
        {
            baseParseBehavior.Parse(xmlData);
        }
    }
}
