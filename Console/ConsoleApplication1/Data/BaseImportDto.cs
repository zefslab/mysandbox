﻿using System;

namespace ConsoleApplication1.Data
{
    abstract public class BaseImportDto
    {
        public string Name;

        public BaseImportDto(string name)
        {
            Name = name;
            Console.WriteLine("Выполняется парсинг " + Name);
        }
    }
}
