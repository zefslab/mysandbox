﻿using System;

namespace ConsoleApplication1.Data
{
    public class PersonDto : BaseImportDto
    {
        public PersonDto(String name) : base(name) { }
    }
}
