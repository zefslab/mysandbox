﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExdendedEnum
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = WeekDays.All.Select(x => new {x.Id, x.DisplayName});
        }
    }

    class WeekDays
    {
        public int Id;

        public bool IsHollyday;

        public string DisplayName;

        WeekDays(bool isHollyday, string displayName)
        {
            IsHollyday = isHollyday;
            DisplayName = displayName;
        }

        public static WeekDays Monday = new WeekDays(false, "Понедельник");
        public static WeekDays Tuesday = new WeekDays(false, "Вторник");
        public static WeekDays Wensday = new WeekDays(false, "Среда");
        public static WeekDays Thirsday = new WeekDays(false, "Четверг");
        public static WeekDays Friday = new WeekDays(false, "Пятница");
        
        //Можно рефлексивно сформировать список
        public static IList<WeekDays> All = new List<WeekDays> {Monday, Tuesday, Wensday, Thirsday, Friday}; 
    }
}
