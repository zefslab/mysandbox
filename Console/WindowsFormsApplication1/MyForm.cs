﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class MyForm<TModel> : Form
    {
        private TModel _model;

        public MyForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(TModel model)
        {
            _model = model;

            return ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
