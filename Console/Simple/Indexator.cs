﻿namespace Simple
{
     class MyIndexator
    {
        private int[] myIndex;

        public int Length {get { return myIndex.Length; }}

        public extern  int Any { get; }
         
        public int this[int index]
        {
            get { return myIndex[index]; }
            set { myIndex[index] = value; }
        }

    }
}
