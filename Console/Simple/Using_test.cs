﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple
{
    /// <summary>
    /// Странное поведение using который не вызывает Dispose как это заявлено при 
    /// возникновении Exception
    ///  </summary>
    class Using_test
    {
        public void Start()
        {
            //try
            //{
                using (MyType s = new MyType())
                {
                    Console.WriteLine("Execute using");
                    s.AnyDo();

                }
            //}
            //catch (Exception)
            //{
                
            //    Console.WriteLine("Эксепшен перехвачен, Dispose вызван");
            //}
            
            Console.ReadKey();
        }

        class MyType : IDisposable
        {
            public void AnyDo()
            {
                throw new Exception();
            }

            public void Dispose()
            {
                Console.WriteLine("Dispose executing");
            }
        }
    }
}
