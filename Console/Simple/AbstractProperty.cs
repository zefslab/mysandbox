﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple
{
    abstract class Systematization
    {
        protected delegate string GetTypeData();

        protected void Systematizate(GetTypeData getter)
        {
            Console.WriteLine(getter);
        }
    }

    class SystematizationConcret : Systematization
    {
        private string _type;

        protected  string typeData
        {
            get { return _type; }
            set { _type = value; }
        }
    }

}
