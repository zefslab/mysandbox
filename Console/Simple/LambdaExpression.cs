﻿using System;
using System.Collections.Generic;

namespace Simple
{
    class Sportsmen
    {
        public int SportRange;

        public List<int> Medals;
    }

    class LambdaExpression
    {
        private Func<int, int, bool> myfunc = (x, y) => x > y;

        # region Объявления предикатов
        //Через лямбда выражение
        private Predicate<Sportsmen> mypredicate = (x) => x.Medals.Count > 3 && x.SportRange > 2;

        //Через анонимный метод
        private Predicate<Sportsmen> mypredicate2 = delegate(Sportsmen s)
        {
            return s.Medals.Count > 3 && s.SportRange > 2;
        };

       
        //Через обычный метод
        private Predicate<Sportsmen> myPredicate3 = MyPredicate;

        //Через конструктор
        private Predicate<Sportsmen> myPredicate4 = new Predicate<Sportsmen>(MyPredicate);

        #endregion

        LambdaExpression()
        {
            var newFunc = new Func<int, int, bool>(MyFunction);
        }

        bool MyFunction(int x, int y)
        {
            return true;
        }

        static bool MyPredicate(Sportsmen s)
        {
            return s.Medals.Count > 3 && s.SportRange > 2;
        }

    }
}
