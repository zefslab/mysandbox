﻿

namespace MySandbox
{
    public enum MyEnum
    {
        /// <summary>
        /// В машине скорой помощи
        /// </summary>
        InAmbulance = 1,
        /// <summary>
        /// В стационаре: в течении 30 суток после ДТП
        /// </summary>
        Hospital30 = 2,
        /// <summary>
        /// В стационаре: в течении 7 суток после ДТП
        /// </summary>
        Hospital7 = 3,
        /// <summary>
        /// На дому: в течении 30 суток после ДТП
        /// </summary>
        AtHome30 = 4,
        /// <summary>
        /// На дому: в течении 7 суток после ДТП
        /// </summary>
        AtHome7 = 5
    }
}
