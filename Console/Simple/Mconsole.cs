﻿using System;

namespace Simple
{
    public static class Mconsole
    {
        public static void WriteLine(Mstring str)
        {
            var bColor = Console.BackgroundColor;

            var fColor = Console.ForegroundColor;

            Console.BackgroundColor = str.Bcolor;

            Console.ForegroundColor = str.Fcolor;

            Console.WriteLine(str.Str);

            Console.BackgroundColor = bColor;

            Console.ForegroundColor = fColor;

        }
        public static void Write(Mstring str)
        {
            var bColor = Console.BackgroundColor;

            var fColor = Console.ForegroundColor;

            Console.BackgroundColor = str.Bcolor;

            Console.ForegroundColor = str.Fcolor;

            Console.WriteLine(str.Str);

            Console.BackgroundColor = bColor;

            Console.ForegroundColor = fColor;

        }
    }
}
