﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Simple
{
    /// <summary>
    /// Следует получше разобраться с асинхронными вызовами
    /// Пример пока не доработан
    /// </summary>
    class  AsyncAwait
    {
        public async void AsyncTask()
        {
            var a = "";


            Task<string> task = new Task<string>(TaskMethod);

            var c = await task;

            var b = 1234;

            Console.WriteLine("Завершился выполняться метод с асинхронным вызовом");
        }

        private string TaskMethod()
        {
            Console.WriteLine("Начало задержки");

           Task.Delay(10000);

            Console.WriteLine("Выполнился асинхронный метод с задержкой");
           
            // throw new Exception("Error in async execution");

            return "";
        }
    }
}
