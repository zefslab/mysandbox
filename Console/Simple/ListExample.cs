﻿using System.Collections.Generic;

namespace Simple
{
    class People
    {
        public int Prop1 = 1;
    }
    class Children : People
    {
        public int Prop2 = 2;
    }
    class Father: People
    {
        public int Prop3 = 3;
    }
    
    class ListExample : List<People>
    {
    }
}
