﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using Simple;
using Simple.Linq;
using linq = Simple.Linq.LinqToXml;

namespace MySandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            ReportToWord.ToWord();

            var a = new  A[] {new A {count = 2}, new A { count = 4 }, new A { count = 10 }, new A { count = 20 } };

            IQueryable<A> test = new EnumerableQuery<A>(Enumerable.Empty<A>());

            var result = test.Where(_a => _a.count > 9);

            var children = new C();
            var parent = children as A;
            if (children != null)
            {
                Console.WriteLine("Ок");
            }

            MyStream.WorkWithStreamWriter();

            Console.WriteLine("Programm is finished. Press any key.");
            Console.ReadKey();
        }
       
    }

    class A
    {
        public int count;
    }
    class C : A { }
}
