﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Simple.Linq
{
    /// <summary>
    /// Примеры выполнения методов расширения и работы с Xml
    /// </summary>
    public static class LinqToXml
    {
        public static XElement CreateXml()
        {
            return new XElement("Position",
                new XElement("Specialty", "Manager") ,
                new XAttribute("code", "2345"),
                new XComment("Мой комментарий")
                
                );
        }
        static public void GetValue()
        {
            var xml = new XElement("table", 43534);

            Console.WriteLine(((int)xml).GetType().Name);
        }

        static public void GenerateXml(IEnumerable<Person> persons)
        {
            var xPersons = new XElement("persons",
                persons.Select(x => new XElement("person",
                    new XElement("Name", x.Name),
                    new XElement("LastName", x.LastName),
                    new XElement("SerName", x.SerName)
                    )));
            Console.WriteLine(xPersons);

        }

        static public void RemoveAllId()
        {
            var xml = Data.ReadDataFromFile();
            if (xml != null)
            {
                Console.WriteLine("Файл считан");
            }

            ///Удаление всех элементов с наименованием Id из всего xml дерева
            xml.Descendants().Where(x => x.Name == "Id").Remove();

            xml.Save("..\\..\\..\\data\\DTP_new.xml");

            Console.WriteLine("Файл обработан и записан");
        }

        static public void XmlEvents()
        {
           var xml =  Data.ReadDataFromFile();

            var IdNodes = xml.Descendants("Id");

            //Добавлен обработчик в виде лябда выражения при изменении элемента идентификатора
            IdNodes.ToList().ForEach(x => x.Changed += (y, e) =>
            {
                string msg = String.Empty;

                if (e == XObjectChangeEventArgs.Value)
                {
                    msg = "Изменяется";
                }
                else if (e == XObjectChangeEventArgs.Remove)
                {
                    msg = "Удаляется";
                }
                else if (e == XObjectChangeEventArgs.Add)
                {
                    msg = "Добавляется";
                }
                    Console.WriteLine("{1} идентификатор со значением: {0}", y.ToString(), msg);
                    
                Console.ReadKey();
            });

            IdNodes.ToList().ForEach(x =>
            {
                if (x.Value == "")
                {
                    //А здесь ожиданось событие XObjectChangeEventArgs.Remove но его не происходит
                    x.Remove();
                }
                else
                {
                    //Как ни странно, но при изменении значения возникают события удаления, а затем добавления
                    //нового значения. Лично я ожидал возникновения события XObjectChangeEventArgs.Value
                    //Т.е. изменение знаячения происходит за два события: Удаление и добавление значения.
                    x.SetValue("2");
                }
                
            });

        }

        static public XmlSchemaSet CreateXsd(XElement xml)
        {
            xml.Save("test.xml");

            var schemaSet = new XmlSchemaInference().InferSchema(new XmlTextReader("test.xml"));

            var writer = XmlWriter.Create("test.xsd");
            
            foreach (XmlSchema schema in schemaSet.Schemas())
            {
                schema.Write(writer);
            }
            
            writer.Close();

            return schemaSet;
        }

        static public bool CheckXmlByXsd( XElement xml, XmlSchemaSet xmlSchemaSet)
        {
            xml.Save("temp.xml");

            var xDocument = XDocument.Load("temp.xml");

            xDocument.Elements().First().AddAfterSelf(new XElement("Error", "error"));

           try
           {

               xDocument.Validate(xmlSchemaSet, null);
               
               Console.WriteLine("Валидация прошла успешно.");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return false;
            }

            return true;
        }
    }
}
