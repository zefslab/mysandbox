﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Simple.Linq
{
    /// <summary>
    /// Примеры работы методов Linq
    /// </summary>
    static class LinqToObjects
    {
        static public void MakeSelect(IEnumerable<Person> person, IEnumerable<SocialStatus> status)
        {
            var _status = status.ToDictionary(x => x.Key, x=>x.Name);
            var result = person.Select(x => new
            {
                Name = x.Name,
                Status = _status[x.SocialStatusKey]
            }).ToList();

            result.ForEach(x=>Console.WriteLine(x.Name + " "+ x.Status));
        }

        static public void MakeJoin(IEnumerable<Person> outer, IEnumerable<SocialStatus> inner)
        {
            var result = outer.Join(inner, p => p.SocialStatusKey, s => s.Key, (p, s) => new
            {
                personName = p.Name,
                socialStatus = s.Name
            });

            result.ToList().ForEach(p => Console.WriteLine("{0} {1}", p.personName, p.socialStatus));

        }
        static public void MakeGroupJoin(IEnumerable<SocialStatus> outer, IEnumerable<Person> inner)
        {
            var result = outer.GroupJoin(inner, s => s.Key, p => p.SocialStatusKey, (s, p) =>
            {
                return new
                {
                    socialStatus = s.Name,
                    personsName = String.Join(", ", p.Select(person => person.Name))
                };

            });

            result.ToList().ForEach(r => Console.WriteLine("{0} => {1}", r.socialStatus, r.personsName));

        }
        static public void MakeGroupBy(IEnumerable<Person> persons)
        {
            persons.GroupBy(p => p.Name + p.LastName)
                .ToList().ForEach(p =>
                {
                    Console.WriteLine(p.Key);
                    p.ToList().ForEach(x => Console.WriteLine("{0} {1} {2}", x.Name, x.SerName, x.LastName));
                });
        }
        static public void MakeUnion(IEnumerable<Person> persons)
        {
            var ferst = persons.Take(3).Select(p => p.Name);
            var second = persons.Skip(3).Select(p => p.Name);

            var union = ferst.Union(second);

            Console.WriteLine("Первая последовательность");
            ferst.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine("Вторая последовательность");
            second.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine("Результирующая последовательность");
            union.ToList().ForEach(p => Console.WriteLine(p));
        }
        static public void RangeAndRepeat()
        {
            Console.WriteLine("Cгенерированная последовательность");

            Enumerable.Range(100, 10).ToList().ForEach(x => Console.WriteLine(x));
            Console.WriteLine("Повторение");
            Enumerable.Repeat(new Person("Иван", "Петрович", "Митяев", 0), 5).ToList().ForEach(x => Console.WriteLine(x.LastName));
        }

        static public void ToDictionary(IEnumerable<Person> persons)
        {
            var dictionary = persons.ToDictionary(x => x.Name + x.SerName + x.LastName, p => p.SerName);
            foreach (var item in dictionary)
            {
                Console.WriteLine(item.Key + " " + item.Value);
            }

        }
        static public void ToLookup(IEnumerable<Person> persons)
        {
            var lookup = persons.ToLookup(x => x.Name);

            foreach (var item in lookup)
            {
                Console.WriteLine(item.Key);

                item.ToList().ForEach(x => Console.WriteLine(x.ToString()));
            }

        }

        static public void MakeDistinct(IEnumerable<Person> persons)
        {
            var resultList = persons.Distinct(new PersonEqalityComparer());

            resultList.Select(p => p.Name).ToList().ForEach(Console.WriteLine);
        }
        //Компоратор для метода MakeDistinct
        class PersonEqalityComparer : IEqualityComparer<Person>
        {
            public bool Equals(Person x, Person y)
            {
                return x.Name.ToUpper().Equals(y.Name.ToUpper());
            }

            public int GetHashCode(Person obj)
            {
                return obj.Name.ToUpper().GetHashCode();
            }
        }

        static public void MakeSequenceEqual()
        {
            var first = new[] { 1, 2, 3 };
            var second = new[] { 1, 2, 3 };
            Console.WriteLine("Последовательности {0} ", first.SequenceEqual(second) ? " равны" : "неравны");
        }

        static public Person GetSingle(IEnumerable<Person> persons, string name)
        {
            var person = new Person();

            try
            {
                person = persons.SingleOrDefault(p => p.Name == name);
                if (person == null)
                {
                    Console.WriteLine("Не найдено ни одной персоны с таким имененм");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Более одного элемента впоследовательности");
            }

            return person;
        }

        static public void MakeSumming(IEnumerable<Person> persons)
        {
            var summ = persons.Sum(x => x.SocialStatusKey);

            Console.WriteLine("Сумма индексов социальных статусов {0} ", summ);
        }

        public static void  TestChunking(int listSize, int chunkSize, Random rnd)
        {
            var array = new double[listSize];

            for (var i = 0; i < listSize; i++)
            {
                array[1] = rnd.NextDouble();
            }

            var chunks = array.ChunkBy(chunkSize).ToList();

            var puf =  chunks.AsParallel().Select((chunk, i) =>
            {
                Console.WriteLine(i);
                var muf = chunk.Select((item, j) =>
                {
                    Console.WriteLine(String.Format("   {0} {1}", j, item));
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    return item;
                }).ToList();
                return i;
            }).ToList();

        }

    }
}
