﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace Simple
{
    public static class Data
    {
        static public string[] PersonNames = new[] {"Anton", "Ivan", "Betty", "Kristy"};

        static public XDocument ReadDataFromFile(string file = "..\\..\\..\\data\\DTP.xml")
        {
            if (new FileInfo(file).Exists)
            {
                return XDocument.Load(file);
            }
            Console.WriteLine("Файл с данными xml не найден в каталоге {0}", new DirectoryInfo(".").FullName );
            return null;
        }

        
        static public List<Person> Persons = new List<Person>()
        {
            new Person("Иван", "Петрович", "Митяев", 0),
            new Person("Игорь", "Петрович", "Кличко", 1),
            new Person("ИВан", "Григорьевич", "Митяев", 2),
            new Person("Игорь", "Никифорович", "Кличко", 4),
            new Person("ИваН", "Иванович", "Осталопов", 3)
        };

        static public List<SocialStatus> SocialStatuses = new List<SocialStatus>()
        {
            new SocialStatus(1, "Нормальный гражданин" ),
            new SocialStatus(2, "Дети до 15 лет" ),
            new SocialStatus(3, "Ветеран войны" ),
            new SocialStatus(4, "Ветеран труда" ),
            new SocialStatus(0, "Инвалид 3 категории" ),
        };
    }

    public class Person
    {
        public string Name { get; set; }
        public string SerName { get; set; }
        public string LastName { get; set; }
        public int SocialStatusKey { get; set; }

        public Person()
        {}

        public Person(string name, string sername, string lastname, int socialStatusKey)
        {
            Name = name;
            SerName = sername;
            LastName = lastname;
            SocialStatusKey = socialStatusKey;

        }

        public override string ToString()
        {
            return String.Format("{0} {1} {2}", LastName, Name, SerName);
        }
    }

    public  class SocialStatus
    {
        public int Key { get; set; }
        public string Name { get; set; }

        public SocialStatus(int key, string name)
        {
            Key = key;

            Name = name;
        }
    }
}
