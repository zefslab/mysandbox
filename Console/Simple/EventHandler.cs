﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Simple
{
    public class Publisher
    {
        private int _speed;
        /// <summary>
        /// Кастомный тип обработчика события
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        public delegate void CustomEventHandler(object o, EventArgs e, int i);


        /// <summary>
        /// Публикация события
        /// </summary>
        public event CustomEventHandler SpeedFaluer;
        
        /// <summary>
        /// Некая процедура выполняемая пуликатором в процессе которой может возникнуть опубликованное событие
        /// </summary>
        public void SpeedUp(int speed, int limit)
        {
            _speed = speed;

            Console.WriteLine($"Скорость: {_speed}");

            if (_speed > limit)
            {
                SpeedFaluer(this, new EventArgs(), speed);
            }
        }

        public override string ToString()
        {
            return "Публикатор события развил скорость до указанного лимита: " + _speed;
        }
    }

    public class Subscriber
    {
        public Subscriber(Publisher publisher)
        {
            //Связывание события с обработчиком
            publisher.SpeedFaluer += Handler_CallBak;
            //Реакция на событие реализуется в лябда выражении
            publisher.SpeedFaluer += (o, a, i) =>
            {
                //Do any thing
            };
            //Реакция на событие организуется добавлением ананимного метода
            publisher.SpeedFaluer += delegate(object o, EventArgs a, int i)
            {
                //Do any thing
            };
        }

        /// <summary>
        /// Обработчик события, на которое подписывается подписчик
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        void Handler_CallBak(object o, EventArgs e, int i)
        {
            Console.WriteLine($"Обработчик поймал событие и обработал переданный обект {o.ToString()}");
            Console.ReadKey();
        }
    }

    /// <summary>
    /// Используется в Programm для демонстрации работы обработки событий
    /// </summary>
    public static class EventHandlingDemonstration
    {
        public static void Start(int spiddLimit)
        {
            var publisher = new Publisher();

            var subscriber = new Subscriber(publisher);

            ///Разгоняем публикатора события до установленного предела
            for (int i = 0; i < 100000000; i++)
            {
                
                publisher.SpeedUp(i, spiddLimit);
            }
            
        }
    }
}
