﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Simple
{
    class MyStream
    {
        private static string file = "c:\\temp\\test.txt";

        public static void WorkWithStream()
        {
            using (Stream s = new FileStream(file, FileMode.Create, FileAccess.ReadWrite))
            {
                Console.WriteLine($"Есть возможность записи: {s.CanWrite}");
                Console.WriteLine($"Есть возможность чтения: {s.CanRead}");

                s.Write(new byte[] {134, 145, 167, 223}, 0, 4);

                s.WriteByte(245);

                s.Position = 0;
                var data = new byte[s.Length];

                s.Read(data, 0, (int)s.Length);
            }
        }

        public static void WorkWithStreamWriter()
        {
            using (StreamWriter sw = new StreamWriter(file))
            {
                sw.Write("Это мой простой текст");
            }

            using (StreamReader sr = new StreamReader(file))
            {
                Console.WriteLine($"I read from {file} this text: " + sr.ReadToEnd());
            }

            StringReader sreader = new StringReader("It is my long text");

            TextReader tr = new StreamReader(new MemoryStream(new byte[] { 1, 3, 4, 67 }));

            using (BinaryWriter bw = new BinaryWriter(new FileStream(file, FileMode.OpenOrCreate, FileAccess.Write)))
            {
                bw.Write(1.34D);
            }

            using (BinaryReader br = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read)))
            {
                var data = br.ReadDouble();
            }


        }
    }
}
