﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadDbContext
{
    class Program
    {
        static DbContextFactory factory = new DbContextFactory();

        static void Main(string[] args)
        {
            var thread1 = new Thread(_contextCreatorForThread);
            var thread2 = new Thread(_contextCreatorForThread);
            var thread3 = new Thread(_contextCreatorForThread);

            thread1.Start();
            thread2.Start();
            thread3.Start();

            var container = factory.GetContextContainer();

            Console.ReadKey();
        }

        private static void _contextCreatorForThread()
        {
            var context1 = factory.Get<MyFirstDbContext>(Thread.CurrentThread.ManagedThreadId);
            var context2 = factory.Get<MySecondDbContext>(Thread.CurrentThread.ManagedThreadId);
            var context3 = factory.Get<MyThirdDbContext>(Thread.CurrentThread.ManagedThreadId);
            var context4 = factory.Get<MyFirstDbContext>(Thread.CurrentThread.ManagedThreadId);
            var context5 = factory.Get<MySecondDbContext>(Thread.CurrentThread.ManagedThreadId);
            var context6 = factory.Get<MyThirdDbContext>(Thread.CurrentThread.ManagedThreadId);

            if (context1.GetHashCode() == context4.GetHashCode()
                && context2.GetHashCode() == context5.GetHashCode()
                && context3.GetHashCode() == context6.GetHashCode())
            {
                lock (factory)
                {
                    Console.WriteLine("Все верно для потока {0}", Thread.CurrentThread.ManagedThreadId);
                }
            }
            else
            {
                lock (factory)
                {
                    Console.WriteLine("Сбой для потока {0}", Thread.CurrentThread.ManagedThreadId);
                }
            }
        }
    }
}
