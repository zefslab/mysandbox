﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadDbContext
{
    public class DbContextFactory
    {
        private  Dictionary<int, IList<DbContext>> _container = new Dictionary<int, IList<DbContext>>();
        
        /// <summary>
        /// Запрос контейнера по его типу и идентификатору потока
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="threadId"></param>
        /// <returns></returns>
        public DbContext Get<TContext>(int threadId)
            where TContext : DbContext, new()
        {
            lock (_container)
            {
                DbContext context;

                if (_container.ContainsKey(threadId))
                {
                    //Коллекция контекстов для потока уже была создана
                    context = _container[threadId].FirstOrDefault(_context => _context.GetType() == typeof (TContext));
                    if (context != null)
                    {
                        //Контекст указаного типа ранее уже использовался потоком
                        //TODO проверить размер на допустимое значение
                        return context;
                    }
                    else
                    {
                        //К контексту данного типа еще ни разу небыло обращения в потоке
                        context = new TContext();

                        _container[threadId].Add(context);

                        return context;
                    }
                }
                else
                {
                    //Добавляется новая коллекция контекстов для потока, из которого пришло первое обращение к контексту
                    context = new TContext();
                    _container.Add(threadId, new List<DbContext>() {context});
                    return context;
                }
            }
        }

        /// <summary>
        /// Указание на закрытие контекста.
        /// Эта команда может так же использоваться для пересоздания контекста в два этапа.
        /// Этой командой контекст закроется, а Get командой опять заново создасться
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="threadId"></param>
        /// <returns></returns>
        public bool CloseContext<TContext>(int threadId)
        {
            lock (_container)
            {
                if (_container.ContainsKey(threadId))
                {
                    var context =
                        _container[threadId].FirstOrDefault(_context => _context.GetType() == typeof (TContext));
                    if (context != null)
                    {
                        _container[threadId].Remove(context);
                        return true;
                    }

                }

                return false;
            }
        }

        /// <summary>
        /// При запкрытии потока будут уничножены все контексты с ним связанные
        /// </summary>
        public void ThreadClosed(int threadId)
        {
            lock (_container)
            {
                if (_container.ContainsKey(threadId))
                {
                    _container.Remove(threadId);
                }
            }
        }

        public Dictionary<int, IList<DbContext>> GetContextContainer()
        {
            lock (_container)
            {
                return _container;
            }

        }
    }
}
