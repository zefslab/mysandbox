﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class ProjectDto :BaseDto
    {
        public string Name { get; set; }

        public int Old { get; set; }
    }
}
