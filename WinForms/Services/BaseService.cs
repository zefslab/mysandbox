﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Services
{
    public class BaseService<T>
        where T : BaseDto, new()
    {
        
        public virtual T Create(T dto)
        {


            return dto;
        }

        public virtual T Update(T dto)
        {


            return dto;
        }
    }
}
