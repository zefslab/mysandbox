﻿
namespace WinForms
{
    partial class ProjectView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtName = new System.Windows.Forms.TextBox();
            this.numOld = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.projectViewBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.projectViewBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.projectViewBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.projectViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numOld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(16, 15);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(132, 22);
            this.txtName.TabIndex = 0;
            this.txtName.Tag = "";
            // 
            // numOld
            // 
            this.numOld.Location = new System.Drawing.Point(16, 71);
            this.numOld.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numOld.Maximum = new decimal(new int[] {
            -1486618624,
            232830643,
            0,
            0});
            this.numOld.Name = "numOld";
            this.numOld.Size = new System.Drawing.Size(160, 22);
            this.numOld.TabIndex = 1;
            this.numOld.Tag = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 137);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 2;
            this.button1.Text = "Старт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // projectViewBindingSource1
            // 
            this.projectViewBindingSource1.DataSource = typeof(WinForms.ProjectView);
            // 
            // projectViewBindingSource2
            // 
            this.projectViewBindingSource2.DataSource = typeof(WinForms.ProjectView);
            // 
            // projectViewBindingSource3
            // 
            this.projectViewBindingSource3.DataSource = typeof(WinForms.ProjectView);
            // 
            // projectViewBindingSource
            // 
            this.projectViewBindingSource.DataSource = typeof(WinForms.ProjectView);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 185);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 3;
            this.button2.Text = "Стоп";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ProjectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 321);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.numOld);
            this.Controls.Add(this.txtName);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ProjectView";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ProjectView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numOld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectViewBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.NumericUpDown numOld;
        private System.Windows.Forms.BindingSource projectViewBindingSource;
        private System.Windows.Forms.BindingSource projectViewBindingSource3;
        private System.Windows.Forms.BindingSource projectViewBindingSource1;
        private System.Windows.Forms.BindingSource projectViewBindingSource2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.IO.Ports.SerialPort serialPort1;
    }
}

