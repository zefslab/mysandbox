﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Services;

namespace WinForms
{
    public partial class ProjectView : Form
    {
        
        public ProjectDto Project { get; set; }

        ProjectService service = new ProjectService();

        private Action<ProgressBar> SetProgerssBar;

        public ProjectView(ProjectDto project)
        {
            InitializeComponent();
            
            Project = project;

        }

        private void ProjectView_Load(object sender, EventArgs e)
        {
            SetProgerssBar = (bar) =>
            {
                bar.Value++;
            };

            txtName.DataBindings.Add("Text", Project, "Name");
            numOld.DataBindings.Add("Value", Project, "Old");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //service.Create(Project);


        }
    }
}
