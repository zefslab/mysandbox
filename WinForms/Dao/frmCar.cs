﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao.Constant;

namespace Dao
{
    public partial class frmCar : Form
    {
        private Car _car;

        public frmCar(Car car)
        {
            InitializeComponent();
            _car = car;
        }
        private void frmCar_Load(object sender, EventArgs e)
        {
            txtMark.Text = _car.Mark;
            txtModel.Text = _car.Model;
            numPower.Value = _car.Power;
            numValue.Value = _car.Value;
            numPrice.Value = _car.Price;
            numWheelInch.Value = _car.WheelInch;
            cmbColor.SelectedValue = _car.Color;

            FillColor();
        }

        private void FillColor()
        {
            cmbColor.DataSource = null;
            cmbColor.ValueMember = "Id";
            cmbColor.DisplayMember = "Color";
            cmbColor.DataSource = Enum.GetNames(typeof (CarColor)).Select((x, i) => new
            {
                Id = i,
                Color = x
            }).ToList();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ///добавление клиента в контекст
            _car.Mark = txtMark.Text;
            _car.Model = txtModel.Text;
            _car.Power = (int)numPower.Value;
            _car.Value = (int)numValue.Value;
            _car.Price = numPrice.Value;
            _car.WheelInch = (int)numWheelInch.Value;
            _car.Color = (int)cmbColor.SelectedValue;

            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

       
    }
}
