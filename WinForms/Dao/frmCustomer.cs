﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dao
{
    public partial class frmCustomer : Form
    {
        private Customer _customer;
       

        public frmCustomer(Customer customer )
        {
            if (customer == null) throw new ArgumentNullException("customer");

            InitializeComponent();
            _customer = customer;
           
        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {
            txtName.Text = _customer.Name;
            txtWork.Text = _customer.Work;
            numOld.Value = _customer.Old ?? 0;

        }
        /// <summary>
        /// Сохранение изменений заказчика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            ///добавление клиента в контекст
            _customer.Name = txtName.Text;
            _customer.Old = (int)numOld.Value;
            _customer.Work = txtWork.Text;
            
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
