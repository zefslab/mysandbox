﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace Dao
{
    public class MyDbConnection : DataContext
    {
        public Table<Customer> Customers
        {
            get { return GetTable<Customer>(); }
            
        }

        public Table<Car> Cars
        {
            get { return GetTable<Car>(); }
            
        }

        public Table<Order> Orders
        {
            get { return GetTable<Order>(); }

        }

        public MyDbConnection()
            : base(@"Server=localhost\SQLEXPRESS;Database=Customers;Integrated Security=SSPI;", new AttributeMappingSource())
        {
        }
    }
}
