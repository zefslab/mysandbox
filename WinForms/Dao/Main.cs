﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dao
{
    public partial class Main : Form
    {
        MyDbConnection _db = new MyDbConnection();

        private Table<Customer> _customers;
        private Table<Car> _cars;
        private Table<Order> _orders;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            _customers = _db.Customers;
            _cars = _db.Cars;
            _orders = _db.Orders;

            gridCars.AutoGenerateColumns = true;
            gridOrders.AutoGenerateColumns = true;

            CarsRefresh();
            CustomersRefresh();
            OrdersRefresh();
        }

        #region Связывание визуальных элементов с DataContext
        private void CustomersRefresh()
        {
            listCustomers.DataSource = null;
            listCustomers.DisplayMember = "Name";
            listCustomers.ValueMember = "Id";
            listCustomers.DataSource = _customers.Select(x => new
                                        {
                                            x.Id,
                                            x.Name
                                        });
        }

        private void OrdersRefresh()
        {
            gridOrders.DataSource = null;
            gridOrders.DataSource = _orders.Select(x => new
            {
                x.Id,
                Customer = _customers.Single(y => y.Id == x.CustomerId).Name,
                Car = _cars.Single(y => y.Id == x.CarId),
                x.Count,
                x.Summ
            });
        }

        private void CarsRefresh()
        {
            gridCars.DataSource = null;
            gridCars.DataSource = _cars;
        }
        #endregion

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            var customer = new Customer();
            
            if (new frmCustomer(customer).ShowDialog() == DialogResult.OK)
            {
                _customers.InsertOnSubmit(customer);
                
                _db.SubmitChanges();

                CustomersRefresh();
            }
        }

        private void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            //Проверить, есть заказы с удаляемым пользователем
            if (_orders.Any(x => x.CustomerId == (int) listCustomers.SelectedValue))
            {
                MessageBox.Show("Нельзя удалить клиента имеющего заказы. Нужно удалить сначала все его заказы.",
                    "Запрет удаления клиента", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var result = MessageBox.Show("Вы уверены, что хотите удалить клиента?",
                    "Удаление клиента", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.No) return;

            var customer = _customers.FirstOrDefault(x =>
                                x.Id == (int) listCustomers.SelectedValue);
            if (customer != null)
            {
                _customers.DeleteOnSubmit(customer);
                _db.SubmitChanges();
                CustomersRefresh();
            }

        }


        private void btnEditCustomer_Click(object sender, EventArgs e)
        {

            var result = new frmCustomer(_customers.FirstOrDefault(x => 
                                                x.Id == (int)listCustomers.SelectedValue))
                                        .ShowDialog();
            if (result == DialogResult.OK)
            {
                //синхронизация контекста с БД
                _db.SubmitChanges();

                //обновление элементов управления
                CustomersRefresh();
                OrdersRefresh();
            }
        }

        private void btnAddCar_Click(object sender, EventArgs e)
        {
            var car = new Car();

            if (new frmCar(car).ShowDialog() == DialogResult.OK)
            {
                _cars.InsertOnSubmit(car);

                _db.SubmitChanges();

                CarsRefresh();
            }
        }

        private void btnAddOrder_Click(object sender, EventArgs e)
        {
                //Api Ado.Net + Sql Provider -> db
            
                //Api LinqToSql -> MsSql Server
            
                //EntityFramework 

        }
    }
}
