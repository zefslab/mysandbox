﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao
{
    [Table (Name = "Customer")]
    public class Customer
    {
        [Column (Name="Id", IsPrimaryKey = true, IsDbGenerated = true) ]
        public int Id { get; set; }

         [Column(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        [Column]
         public int? Old { get; set; }

        /// <summary>
        /// Mesto работы
        /// </summary>
        [Column]
        public string Work { get; set; }
    }
}
