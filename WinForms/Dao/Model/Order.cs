﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao
{
    [Table]
    public class Order
    {
        [Column(Name = "Id", IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column]
        public int CustomerId { get; set; }
        
        [Column]
        public int CarId { get; set; }
        
        [Column]
        public int Count { get; set; }
        
        [Column]
        public decimal Summ { get; set; }
    }
}
