﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao
{
    [Table (Name = "Car")]
    public class Car
    {
        private string _fullCarSpecification;

        [Column(Name = "Id", IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column]
        public string Mark { get; set; }

        [Column]
        public string Model { get; set; }

        [Column(Name = "Value")]
        public int Value { get; set; }

        [Column(Name = "Power")]
        public int Power { get; set; }

        [Column]
        public decimal Price { get; set; }

        [Column]
        public int WheelInch { get; set; }

        [Column]
        public int Color { get; set; }

        public string FullCarSpecification
        {
            set { _fullCarSpecification = String.Format("Марка: {0} Модель {1}  Мощность{2}", Mark, Model, Power); }
        }


        public override string ToString()
        {
            return String.Format("{0} {1}",Mark, Model);
        }
    }
}
