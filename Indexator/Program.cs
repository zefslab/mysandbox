﻿using System;
using System.Collections.Generic;

namespace Indexator
{
    class Program
    {
        static void Main(string[] args)
        {
            var piolpe = new Piople();
            piolpe["Ivan"] = new Person {Name = "Ivan", Address = "Kemerovo"};
            piolpe["Galy"] = new Person { Name = "Galy", Address = "Novosib" };

            
            Console.WriteLine($"Ivan - {piolpe["Ivan"].Address}");
            Console.WriteLine($"Galy - {piolpe["Galy"].Address}");
            Console.ReadKey();
        }
    }

    class Person
    {
        public string Name { get; set; }

        public string Address { get; set; }
    }

    class Piople
    {
        private IDictionary<string, Person> data = new Dictionary<string, Person>();
        public Person this[string name] 
        {
            get { return data[name]; }
            set { data.Add(name, value); }
        }
    }
}
