﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Enumerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //неименовованный энумератор
            foreach(var item in new MyCollection())
            {
                Console.WriteLine(item);
            }

            //именованный энумератор
            foreach (string item in (new MyCollection()).GetAll())
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }

        class MyCollection  
        {
            //Возвращает IEnumerator
            public IEnumerator<string> GetEnumerator()
            {
                for (int i = 0; i < 6; i++)
                {
                    yield return (i * i).ToString();
                }
            }

            //возвращает IEnumerable
            public IEnumerable<string> GetAll()
            {
                for (int i = 0; i < 6; i++)
                {
                    yield return (i * i).ToString();
                }
            }
        }
    }
}
