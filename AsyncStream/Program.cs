﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Action<string[], Random> action = async (mass, rdm) =>
            {
                var repo = new Repository(mass);
                await foreach (var item in repo.GetDataAsync(rdm))
                {
                    Console.WriteLine($"Обрабатываем {item.number} элемент с задержкой {item.daly}");
                }
            };
            Random rd = new Random(2000);

            Task.Run(async () => action(new[] { "1", "2", "3", "4", "5" }, rd));
            
            Task.Run(async () => action(new[] { "6", "7", "8", "9", "10" }, rd));
            
            Console.ReadKey();
        }
    }

    class Repository
    {
        string[] data;

        public Repository(string[] data)
        {
            this.data = data;
        }
        public async IAsyncEnumerable<(string number, int daly)> GetDataAsync(Random rdm)
        {
            for (int i = 0; i < data.Length; i++)
            {
               
                var daly = rdm.Next(3000);
                await Task.Delay(daly);
                yield return (data[i], daly);
            }
        }
    }
}
